# JavaScript Project Skelton

This is a simple raw JavaScript project generator with Babel and Gulp.

## Install NPM & Dependencies

Install Node, clone this repo, and then run:

```
npm install -g gulp eslint
```

## Configure

```
npm init
npm install --save-dev gulp-babel gulp-eslint @babel/core @babel/preset-env
```

### Optionally generate a new .eslint.js

```
eslint --init
```

You also might want to tweak *.eslintrc.js* to not complain about console.logs and include jquery:

```
module.exports = {
"env": {
        "jquery": true,
    },
    rules: {
        'no-console': 'off',
    },
};
```


## To Build from es6 files run:

```
gulp
```

This will transcompile any .js files in es6 or public/es6 into js or public/js.
